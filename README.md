                   _      __
      ___ ________(_)__  / /____
     (_-</ __/ __/ / _ \/ __(_-<
    /___/\__/_/ /_/ .__/\__/___/
                 /_/

dirty little helpers.


## bat

shows battery capacity and sets charge thresholds.
simple wrapper around [tpacpi-bat](https://github.com/teleshoes/tpacpi-bat).

```sh
$ bat --help
usage: bat [ d[efault] | 0-95 ]

$ bat
Remaining: 07:49 [8.0 W]
BAT0: Unknown 97% [25/90]
BAT1: Discharging 97% [25/90]

$ MAX=98 bat 100
Charging: 00:02 [13.0 W]
BAT 0: Charging 97% [98/98]
BAT 1: Unknown 97% [98/98]
```

## blog

create new blog post. needs: `export DIR_BLOG=`

```sh
$ blog Title
```

## figlet-random

figlet wrapper with random font file

```sh
$ figlet-random -D ordin[r
                      o  o
 _   ,_   _|  o        __,   ,_
/ \_/  | / |  | /|/|  /  |  /  |
\_/    |/\/|_/|/ | |_/\_/\_/   |/
```

## scan

scanimage (SANE) wrapper

defaults: 300dpi, color, batchmode

usage: scan [50-9600dpi] [>FILE]

## ssp

screenshot (scrot -s) and paste
usage: `ssp`

## wallpaper

sets a random images as wallpaper

```sh
git clone https://git.christoph-polcin.com/wallpapers ~/.wallpapers
```

```sh
$ wallpaper --help
usage: wallpaper [-d 5m [~/.wallpapers] | <IMAGE>]
```
