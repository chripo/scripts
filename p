#/bin/sh
# christoph-polcin.com

DST="xt"
DSTDIR="paste"
DSTURL="https://p.polcin.de"

if [ -f "$1" ]; then
  f="$1"
  shift
else
  f="$(mktemp -p /tmp XXXXXX)"
  cat > "$f"
fi
	
[ $(stat -c %s "$f") -eq 0 ] && exit 1

[ -z "$1" ] && n="$(basename "$f")" || n="$1"

if file -bL "$f" | grep -q text >/dev/null 2>&1; then
  n="$n.txt"
fi

cat "$f" | ssh $DST "cat > '$DSTDIR/$n'; chmod 664 '$DSTDIR/$n'"

echo "$DSTURL/$n" | xclip
echo "$DSTURL/$n"
