#!/bin/sh
# https://github.com/teleshoes/tpacpi-bat
# christoph-polcin.com

R=/sys/class/power_supply

[ -z "$MAX" ] && MAX=95

info() {
    local pn="$(bc <<< "$(cat $R/BAT*/power_now | tr '\n' '+')0")"
    if [ $pn -gt 0 ]; then
        local en="$(bc <<< "$(cat $R/BAT*/energy_now | tr '\n' '+')0")"
        local action="Remaining"

        if grep -q 1 "$R/AC/online"; then
            local action="Charging"
            local ef="0"
            for i in `ls $R | grep BAT`
            do
                local n="$(cat $R/$i/energy_now)"
                local f="$(cat $R/$i/energy_full)"
                local t="$(cat $R/$i/charge_stop_threshold)"
                local c="$(bc <<< "($f "'*'" $t) / 100")"
                [ $n -gt $c ] && continue

                ef="$ef + $c"
            done
            en="$(bc <<< "($ef) - $en")"
        fi

        local tm="$(bc <<< "scale=4; ($en / $pn) * 60")"
        local h="$(bc <<< "scale=0; $tm / 60")"
        local m="$(bc <<< "($tm % 60)/1")"
        local w="$(bc <<< "$pn / 1000000")"
        printf "%s: %02d:%02d [%.1f W]\n" $action $h $m $w
    fi

    for i in `ls $R | grep BAT`
    do
        info_bat $i
    done
}

info_bat() {
    local b="$R/${1}"
    echo "${1}: $(cat $b/status $b/capacity $b/charge_stop_threshold $b/charge_start_threshold | tr '\n' ' ' | \
        sed 's/\([^ ]*\)\s*\([0-9]*\)\s*\([0-9]*\)\s*\([0-9]*\)/\1 \2% [\3\/\4]/')"
}

_help() {
	echo "usage: $(basename $0) [ de[fault] | 0-$MAX | di[scharge] <NUM> ]"
    exit 1
}

case "$1" in
    [0-9]*)
        if (( 0 <= $1 && $1 < $MAX ))
        then
            sudo tpacpi-bat -s SP 0 $1
            sudo tpacpi-bat -s ST 0 $1
        else
            sudo tpacpi-bat -s SP 0 $MAX
            sudo tpacpi-bat -s ST 0 $MAX
        fi
        sudo tpacpi-bat -s IC 0 1
        sleep 2
    ;;

    default|de*)
        sudo tpacpi-bat -s SP 0 90
        sudo tpacpi-bat -s ST 0 25
        sudo tpacpi-bat -s IC 0 0
        sleep 2
    ;;

	discharge|di*)
		if [ "$2" = "0" ]; then
			sudo tpacpi-bat -s FD 2 0
			sudo tpacpi-bat -s FD 1 1
		elif [ "$2" = "1" ]; then
			sudo tpacpi-bat -s FD 1 0
			sudo tpacpi-bat -s FD 2 1
		else
			_help
		fi
        sleep 2
	;;

    -h|--help|help)
		_help
	;;
esac

info
